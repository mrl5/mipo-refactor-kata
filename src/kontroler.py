# SPDX-License-Identifier: BSD-2-Clause

from src.interfejsy.kontroler import IKontroler
from src.interfejsy.umowa import IUmowa
from src.interfejsy.widok import IWidok


class WynagrodzenieKontroler(IKontroler):
    # wzorzec model-view-controller
    def __init__(self, model: IUmowa, widok: IWidok):
        self._model = model
        self._widok = widok

    @property
    def model(self) -> IUmowa:
        return self._model

    @property
    def widok(self) -> IWidok:
        return self._widok

    def oblicz(self, wynagrodzenieBrutto: float):
        self.model.ustawWynagrodzenieBrutto(wynagrodzenieBrutto)
        self.model.obliczWynagrodzenieNetto()

    def wyswietl(self):
        zrzutowanyModel = self.model.serializujSie()
        self.widok.model = zrzutowanyModel
        self.widok.wyswietl()
