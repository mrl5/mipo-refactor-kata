# SPDX-License-Identifier: BSD-2-Clause

from src.interfejsy.widok import IWidok
from src.widoki.widok import Widok


def procentDoWyswietlenia(liczba: float) -> str:
    procent = liczba * 100
    doWyswietlenia = str(procent).replace(".", ",")
    if doWyswietlenia.endswith(",0"):
        return doWyswietlenia.split(",")[0]
    return doWyswietlenia


class CliStaryWidok(Widok, IWidok):
    def __init__(self, *args):
        super().__init__(*args)
        self._kontrakt = "umowa_widok"

    @property
    def kontrakt(self):
        return self._kontrakt

    @Widok.zwaliduj
    def wyswietl(self):
        model = self.model
        print(model["nazwa"])
        if model["nazwa"] == "UMOWA-ZLECENIE":
            print("Podstawa wymiaru składek", model["wynagrodzenie_brutto"])
        else:
            print("Podstawa wymiaru składek ", model["wynagrodzenie_brutto"])
        print(f"Składka na ubezpieczenie emerytalne {model['skladka_emerytalna']}")
        print(f"Składka na ubezpieczenie rentowe    {model['skladka_rentowa']}")
        print(f"Składka na ubezpieczenie chorobowe  {model['skladka_chorobowa']}")
        print(
            f"Podstawa wymiaru składki na ubezpieczenie zdrowotne:  {model['podstawa_wymiaru_skladki']}"
        )
        print(
            f"Składka na ubezpieczenie zdrowotne: {procentDoWyswietlenia(model['skladka_zdrowotna']['zaplacone']['procent'])}% = {model['skladka_zdrowotna']['zaplacone']['kwota']} {procentDoWyswietlenia(model['skladka_zdrowotna']['dla_zaliczki_na_podatek']['procent'])}% = {model['skladka_zdrowotna']['dla_zaliczki_na_podatek']['kwota']}"
        )
        if model["nazwa"] == "UMOWA-ZLECENIE":
            print(
                f"Koszty uzyskania przychodu (stałe) {model['koszty_uzyskania_przychodu']}"
            )
        else:
            print(
                f"Koszty uzyskania przychodu w wysokości  {model['koszty_uzyskania_przychodu']}"
            )
        print(
            f"Podstawa opodatkowania  {model['podstawa_opodatkowania']['obliczona']}  zaokrąglona {model['podstawa_opodatkowania']['zaokraglona']}"
        )
        print(
            f"Zaliczka na podatek dochodowy {procentDoWyswietlenia(model['zaliczka_na_podatek_dochodowy']['procent'])}% = {model['zaliczka_na_podatek_dochodowy']['kwota']}"
        )
        if (
            "kwota_wolna_od_podatku" in model
            and model["kwota_wolna_od_podatku"] is not None
        ):
            print(f"Kowta wolna od podatku = {model['kwota_wolna_od_podatku']}")
        print(f"Podatek potrącony = {model['podatek']}")
        print(
            f"Zaliczka do urzędu skarbowego = {model['zaliczka_dla_us']['obliczona']} po zaokrągleniu {model['zaliczka_dla_us']['zaokraglona']}"
        )
        print()
        print(
            f"Pracownik otrzyma wynagrodzenie netto w wysokości = {model['wynagrodzenie_netto']}"
        )
