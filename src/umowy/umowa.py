# SPDX-License-Identifier: BSD-2-Clause

from abc import ABCMeta, abstractmethod
from typing import List

from src.interfejsy.rozliczenie import IRozliczenie
from src.interfejsy.serializer import ISerializer
from src.interfejsy.umowa import IUmowa
from src.koszty.chorobowa import SkladkaChorobowa
from src.koszty.emerytalna import SkladkaEmerytalna
from src.koszty.rentowa import SkladkaRentowa
from src.koszty.zaliczka import ZaliczkaDoUrzeduSkarbowego, ZaliczkaNaPodatekDochodowy
from src.koszty.zdrowotna import SkladkaZdrowotna


class PodstawaOpodatkowania(IRozliczenie, ISerializer):
    def __init__(self, podstawaWymiaruSkladki: float, kosztyUzyskaniaPrzychodu: float):
        self._podstawaWymiaruSkladki = podstawaWymiaruSkladki
        self._kosztyUzyskaniaPrzychodu = kosztyUzyskaniaPrzychodu

    @property
    def wartosc(self) -> float:
        return self._podstawaWymiaruSkladki - self._kosztyUzyskaniaPrzychodu

    def zwrocWartoscDoRozliczenia(self):
        return int(round(self.wartosc, 0))

    def serializujSie(self):
        return {
            "obliczona": self.wartosc,
            "zaokraglona": self.zwrocWartoscDoRozliczenia(),
        }


class Umowa(IUmowa, ISerializer, metaclass=ABCMeta):
    def __init__(self, wynagrodzenieBrutto=None):
        self._kontrakt = "umowa_widok"
        self._wynagrodzenieBrutto = wynagrodzenieBrutto
        self._wynagrodzenieNetto = None
        self._kosztyUzyskaniaPrzychodu = None
        self._kwotaZmniejszajacaPodatek = None
        self._wszystkoCoUkradli = []
        self._podstawaWymiaruSkladki = None
        self._podstawaOpodatkowania = None
        self._zaliczkaNaPodatekDochodowy = None
        self._podatekPotracony = None

    @property
    @abstractmethod
    def nazwa(self) -> str:
        pass

    @property
    def kosztyUzyskaniaPrzychodu(self) -> float:
        return self._kosztyUzyskaniaPrzychodu

    @kosztyUzyskaniaPrzychodu.setter
    def kosztyUzyskaniaPrzychodu(self, kosztyUzyskaniaPrzychodu: float):
        self._kosztyUzyskaniaPrzychodu = kosztyUzyskaniaPrzychodu

    @property
    def kwotaZmniejszajacaPodatek(self):
        return self._kwotaZmniejszajacaPodatek

    @property
    def wszystkoCoUkradli(self):
        return self._wszystkoCoUkradli

    @wszystkoCoUkradli.setter
    def wszystkoCoUkradli(self, wszystkoCoUkradli: list):
        self._wszystkoCoUkradli = wszystkoCoUkradli

    @property
    def wynagrodzenieBrutto(self) -> float:
        return self._wynagrodzenieBrutto

    @property
    def wynagrodzenieNetto(self) -> float:
        return self._wynagrodzenieNetto

    @property
    def podstawaWymiaruSkladki(self) -> float:
        return self._podstawaWymiaruSkladki

    @property
    def podstawaOpodatkowania(self) -> PodstawaOpodatkowania:
        return self._podstawaOpodatkowania

    @property
    def zaliczkaNaPodatekDochodowy(self) -> ZaliczkaNaPodatekDochodowy:
        return self._zaliczkaNaPodatekDochodowy

    @property
    def podatekPotracony(self) -> float:
        return self._podatekPotracony

    @property
    def kontrakt(self) -> str:
        return self._kontrakt

    @abstractmethod
    def zestawWszystkoCoUkradli(self) -> None:
        pass

    def ustawWynagrodzenieBrutto(self, wynagrodzenieBrutto):
        self._wynagrodzenieBrutto = wynagrodzenieBrutto

    def obliczPodstaweWymiaruSkladki(self, ubezpieczeniaSpoleczne: List[IRozliczenie]):
        podstawaWymiaruSkladki = self.wynagrodzenieBrutto
        for u in ubezpieczeniaSpoleczne:
            podstawaWymiaruSkladki = (
                podstawaWymiaruSkladki - u.zwrocWartoscDoRozliczenia()
            )
        self._podstawaWymiaruSkladki = podstawaWymiaruSkladki

    def obliczPodstaweOpodatkowania(self):
        self._podstawaOpodatkowania = PodstawaOpodatkowania(
            self.podstawaWymiaruSkladki, self.kosztyUzyskaniaPrzychodu
        )

    def obliczZaliczkeNaPodatekDochodowy(self):
        self._zaliczkaNaPodatekDochodowy = ZaliczkaNaPodatekDochodowy(
            self.podstawaOpodatkowania.zwrocWartoscDoRozliczenia()
        )

    def obliczPodatekPotracony(self, skladkaZdrowotna: SkladkaZdrowotna):
        podatekPotracony = (
            self.zaliczkaNaPodatekDochodowy.wartosc - self.kwotaZmniejszajacaPodatek
        )
        self._podatekPotracony = podatekPotracony

    def zwrocZaliczkeDoUrzeduSkarbowego(
        self, skladkaZdrowotna: SkladkaZdrowotna
    ) -> ZaliczkaDoUrzeduSkarbowego:
        zaliczka = (
            self.zaliczkaNaPodatekDochodowy.wartosc
            - skladkaZdrowotna.zwrocWartoscDlaZaliczkiNaPodatek()
            - self.kwotaZmniejszajacaPodatek
        )
        return ZaliczkaDoUrzeduSkarbowego(zaliczka)

    def obliczWynagrodzenieNetto(self):
        self.zestawWszystkoCoUkradli()
        wszystkoCoUkradli = 0
        for u in self.wszystkoCoUkradli:
            wszystkoCoUkradli += u.zwrocWartoscDoRozliczenia()
        wynagrodzenieNetto = self._wynagrodzenieBrutto - wszystkoCoUkradli
        self._wynagrodzenieNetto = round(wynagrodzenieNetto, 2)

    def wyciagnijKoszt(self, klasa):
        koszt = list(
            filter(
                lambda x: x.__class__.__name__ == klasa.__name__, self.wszystkoCoUkradli
            )
        ).pop()
        return koszt

    def serializujSie(self):
        return {
            "nazwa": self.nazwa,
            "wynagrodzenie_brutto": round(float(self.wynagrodzenieBrutto), 2),
            "skladka_emerytalna": round(
                self.wyciagnijKoszt(SkladkaEmerytalna).zwrocWartoscDoRozliczenia(), 2
            ),
            "skladka_rentowa": round(
                self.wyciagnijKoszt(SkladkaRentowa).zwrocWartoscDoRozliczenia(), 2
            ),
            "skladka_chorobowa": round(
                self.wyciagnijKoszt(SkladkaChorobowa).zwrocWartoscDoRozliczenia(), 2
            ),
            "podstawa_wymiaru_skladki": self.podstawaWymiaruSkladki,
            "skladka_zdrowotna": self.wyciagnijKoszt(SkladkaZdrowotna).serializujSie(),
            "koszty_uzyskania_przychodu": self.kosztyUzyskaniaPrzychodu,
            "podstawa_opodatkowania": self.podstawaOpodatkowania.serializujSie(),
            "zaliczka_na_podatek_dochodowy": self.zaliczkaNaPodatekDochodowy.serializujSie(),
            "kwota_wolna_od_podatku": self.kwotaZmniejszajacaPodatek
            if self.kwotaZmniejszajacaPodatek > 0
            else None,
            "podatek": round(self.podatekPotracony, 2),
            "zaliczka_dla_us": self.wyciagnijKoszt(
                ZaliczkaDoUrzeduSkarbowego
            ).serializujSie(),
            "wynagrodzenie_netto": self.wynagrodzenieNetto,
        }
