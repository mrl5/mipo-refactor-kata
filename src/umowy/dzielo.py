# SPDX-License-Identifier: BSD-2-Clause

from src.koszty.chorobowa import SkladkaChorobowa
from src.koszty.emerytalna import SkladkaEmerytalna
from src.koszty.rentowa import SkladkaRentowa
from src.koszty.zdrowotna import SkladkaZdrowotna
from src.umowy.umowa import Umowa


class UmowaDzielo(Umowa):
    def __init__(self):
        super().__init__()
        self._nazwa = "UMOWA-O-DZIELO"
        self._kwotaZmniejszajacaPodatek = 0

    @property
    def nazwa(self):
        return self._nazwa

    def _obliczKosztyUzyskaniaPrzychodu(self):
        mnoznik = 20 / 100
        kosztyUzyskaniaPrzychodu = self.podstawaWymiaruSkladki * mnoznik
        return kosztyUzyskaniaPrzychodu

    def zestawWszystkoCoUkradli(self):
        ubezpieczeniaSpoleczne = [
            SkladkaEmerytalna(0),
            SkladkaRentowa(0),
            SkladkaChorobowa(0),
        ]
        self.obliczPodstaweWymiaruSkladki(ubezpieczeniaSpoleczne)
        skladkaZdrowotna = SkladkaZdrowotna(0)
        self.kosztyUzyskaniaPrzychodu = self._obliczKosztyUzyskaniaPrzychodu()
        self.obliczPodstaweOpodatkowania()
        self.obliczZaliczkeNaPodatekDochodowy()
        self.obliczPodatekPotracony(skladkaZdrowotna)
        zaliczkaDoUrzeduSkarbowego = self.zwrocZaliczkeDoUrzeduSkarbowego(
            skladkaZdrowotna
        )
        self.wszystkoCoUkradli = self.wszystkoCoUkradli + ubezpieczeniaSpoleczne
        self.wszystkoCoUkradli.append(skladkaZdrowotna)
        self.wszystkoCoUkradli.append(zaliczkaDoUrzeduSkarbowego)
