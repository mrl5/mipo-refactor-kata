# SPDX-License-Identifier: BSD-2-Clause

from src.koszty.chorobowa import SkladkaChorobowa
from src.koszty.emerytalna import SkladkaEmerytalna
from src.koszty.rentowa import SkladkaRentowa
from src.koszty.zdrowotna import SkladkaZdrowotna
from src.umowy.umowa import Umowa


class UmowaPraca(Umowa):
    def __init__(self):
        super().__init__()
        self._nazwa = "Umowa o pracę"
        self._wszystkoCoUkradli = []
        self._kosztyUzyskaniaPrzychodu = 111.25
        self._kwotaZmniejszajacaPodatek = 46.33

    @property
    def nazwa(self):
        return self._nazwa

    def zestawWszystkoCoUkradli(self):
        ubezpieczeniaSpoleczne = [
            SkladkaEmerytalna(self.wynagrodzenieBrutto),
            SkladkaRentowa(self.wynagrodzenieBrutto),
            SkladkaChorobowa(self.wynagrodzenieBrutto),
        ]
        self.obliczPodstaweWymiaruSkladki(ubezpieczeniaSpoleczne)
        skladkaZdrowotna = SkladkaZdrowotna(self.podstawaWymiaruSkladki)
        self.obliczPodstaweOpodatkowania()
        self.obliczZaliczkeNaPodatekDochodowy()
        self.obliczPodatekPotracony(skladkaZdrowotna)
        zaliczkaDoUrzeduSkarbowego = self.zwrocZaliczkeDoUrzeduSkarbowego(
            skladkaZdrowotna
        )
        self.wszystkoCoUkradli = self.wszystkoCoUkradli + ubezpieczeniaSpoleczne
        self.wszystkoCoUkradli.append(skladkaZdrowotna)
        self.wszystkoCoUkradli.append(zaliczkaDoUrzeduSkarbowego)
