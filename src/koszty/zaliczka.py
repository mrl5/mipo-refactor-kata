# SPDX-License-Identifier: BSD-2-Clause

from src.interfejsy.rozliczenie import IRozliczenie
from src.interfejsy.serializer import ISerializer


class ZaliczkaDoUrzeduSkarbowego(IRozliczenie, ISerializer):
    def __init__(self, wartosc: float):
        self._wartosc = wartosc

    def zwrocWartoscDoRozliczenia(self):
        return int(round(self._wartosc, 0))

    def serializujSie(self):
        return {
            "obliczona": round(self._wartosc, 2),
            "zaokraglona": self.zwrocWartoscDoRozliczenia(),
        }


class ZaliczkaNaPodatekDochodowy(IRozliczenie, ISerializer):
    def __init__(self, podstawaOpodatkowania: float):
        self._podstawaOpodatkowania = podstawaOpodatkowania
        self._podatek = self._ustawPodatek()
        self._wartosc = self._obliczWartoscZaliczki(podstawaOpodatkowania)

    @property
    def wartosc(self):
        return self._wartosc

    @property
    def podatek(self):
        return self._podatek

    def _ustawPodatek(self):
        return 18 / 100

    def _obliczWartoscZaliczki(self, podstawaOpodatkowania: float):
        wartosc = podstawaOpodatkowania * self.podatek
        return wartosc

    def zwrocWartoscDoRozliczenia(self):
        return round(self._wartosc, 2)

    def serializujSie(self):
        return {"procent": self.podatek, "kwota": self.zwrocWartoscDoRozliczenia()}
