# SPDX-License-Identifier: BSD-2-Clause

from src.interfejsy.rozliczenie import IRozliczenie
from src.interfejsy.serializer import ISerializer


class SkladkaZdrowotna(IRozliczenie, ISerializer):
    def __init__(self, podstawa: float):
        self._podstawa = podstawa
        self._mnoznik = 9.0 / 100
        self._mnoznikDlaZaliczkiNaPodatek = 7.75 / 100
        self._wartosc = podstawa * self._mnoznik

    def zwrocWartoscDoRozliczenia(self):
        return self._wartosc

    def zwrocWartoscDlaZaliczkiNaPodatek(self):
        wartosc = self._podstawa * self._mnoznikDlaZaliczkiNaPodatek
        return wartosc

    def serializujSie(self):
        return {
            "zaplacone": {
                "procent": self._mnoznik,
                "kwota": round(self.zwrocWartoscDoRozliczenia(), 2),
            },
            "dla_zaliczki_na_podatek": {
                "procent": self._mnoznikDlaZaliczkiNaPodatek,
                "kwota": round(self.zwrocWartoscDlaZaliczkiNaPodatek(), 2),
            },
        }
