# SPDX-License-Identifier: BSD-2-Clause


class NieznanaUmowaError(Exception):
    def __init__(self, msg):
        self.msg = msg
