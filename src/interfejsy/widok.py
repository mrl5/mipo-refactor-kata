# SPDX-License-Identifier: BSD-2-Clause

from abc import ABCMeta, abstractmethod


class IWidok(metaclass=ABCMeta):
    @classmethod
    def __subclasshook__(cls, subclass):
        return hasattr(subclass, "wyswietl") and callable(subclass.wyswietl)

    @abstractmethod
    def wyswietl(self) -> None:
        raise NotImplementedError
