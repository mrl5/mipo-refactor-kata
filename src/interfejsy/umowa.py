# SPDX-License-Identifier: BSD-2-Clause

from abc import ABCMeta, abstractmethod


class IUmowa(metaclass=ABCMeta):
    @classmethod
    def __subclasshook__(cls, subclass):
        return (
            hasattr(subclass, "obliczWynagrodzenieNetto")
            and callable(subclass.obliczWynagrodzenieNetto)
            and hasattr(subclass, "ustawWynagrodzenieBrutto")
            and callable(subclass.ustawWynagrodzenieBrutto)
        )

    @abstractmethod
    def ustawWynagrodzenieBrutto(self, wynagrodzenieBrutto: float) -> None:
        raise NotImplementedError

    @abstractmethod
    def obliczWynagrodzenieNetto(self) -> None:
        raise NotImplementedError
