# SPDX-License-Identifier: BSD-2-Clause

import json
from enum import Enum, unique
from pathlib import Path


@unique
class SchematJsonEnum(Enum):
    umowa_widok = Path(__file__).resolve().parent / "umowa_widok.schema.json"


def pobierzSchematJson(kontrakt: str) -> dict:
    plik = SchematJsonEnum[kontrakt].value
    with open(plik) as f:
        schemat = json.load(f)
    return schemat
