# MIPO refactor kata

This task is about practicing code refactor. We will work on [uber ugly
TaxCalculator.py](https://gitlab.com/mrl5/mipo-refactor-kata/-/blob/66d6413c5d45fca97c50506e14d697f8ab73f7df/TaxCalculator.py)

## UML:

![refactor_uml.svg](./img/refactor_uml.svg)

## Pre-processing reaction:

![preprocessing_reaction.jpg](./img/preprocessing_reaction.jpg)

## Post-processing reaction:

![postprocessing_reaction.png](./img/postprocessing_reaction.png)


## Getting started

1. (optional) create python3 virtual environment
```
VENV_DIR="$HOME/.my_virtualenvs/dev"
mkdir -p "${VENV_DIR}"
python3 -m venv "${VENV_DIR}"
```

2. (optional) switch to new virtualenv:
```
source "${VENV_DIR}/bin/activate"
```

3. Install requirements:
```
pip install -r requirements.txt
```

4. (optional) install pre-commit hooks:
```
pre-commit install
```
