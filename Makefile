SHELL=/bin/bash

.PHONY: all help install lint test

all: help

install:
	pip install -r requirements.txt

lint:
	pre-commit run --all-files

test:
	pytest --cov=src/ test/

help:
	@echo "For available targets type: 'make ' and hit TAB"
	@echo
