# SPDX-License-Identifier: BSD-2-Clause

import pytest

from src.interfejsy.rozliczenie import IRozliczenie
from src.interfejsy.serializer import ISerializer
from src.koszty.zaliczka import ZaliczkaDoUrzeduSkarbowego


@pytest.fixture(scope="function")
def instance():
    o = ZaliczkaDoUrzeduSkarbowego(100)
    return o


@pytest.fixture(scope="function")
def roundingInstance():
    o = ZaliczkaDoUrzeduSkarbowego(123.45678)
    return o


def test_interface_implementation(instance):
    assert isinstance(instance, IRozliczenie) is True
    assert isinstance(instance, ISerializer) is True


def test_correct_value(instance):
    result = instance.zwrocWartoscDoRozliczenia()
    assert result == instance._wartosc


def test_correct_rounding(roundingInstance):
    result = roundingInstance.zwrocWartoscDoRozliczenia()
    assert round(result, 0) == result
