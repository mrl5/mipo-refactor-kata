# SPDX-License-Identifier: BSD-2-Clause

import pytest

from src.interfejsy.rozliczenie import IRozliczenie
from src.interfejsy.serializer import ISerializer
from src.koszty.zdrowotna import SkladkaZdrowotna


@pytest.fixture(scope="function")
def instance():
    o = SkladkaZdrowotna(100)
    return o


def test_interface_implementation(instance):
    assert isinstance(instance, IRozliczenie) is True
    assert isinstance(instance, ISerializer) is True


def test_correct_value(instance):
    result = instance.zwrocWartoscDoRozliczenia()
    assert result == 9.0
