# SPDX-License-Identifier: BSD-2-Clause

from test.widoki.test_cli_stary import get_mock

import pytest

from src.interfejsy.kontroler import IKontroler
from src.kontroler import WynagrodzenieKontroler
from src.umowy.praca import UmowaPraca
from src.umowy.zlecenie import UmowaZlecenie
from src.widoki.cli_stary import CliStaryWidok

PRACA_MOCK = "mock_cli_stary_praca.txt"
ZLECENIE_MOCK = "mock_cli_stary_zlecenie.txt"

controllers_with_old_views = [
    (WynagrodzenieKontroler(UmowaPraca(), CliStaryWidok()), 1337, PRACA_MOCK),
    (WynagrodzenieKontroler(UmowaZlecenie(), CliStaryWidok()), 1337, ZLECENIE_MOCK),
]


@pytest.fixture(scope="function")
def instance():
    model = UmowaPraca()
    widok = CliStaryWidok()
    o = WynagrodzenieKontroler(model, widok)
    return o


def test_interface_implementation(instance):
    assert isinstance(instance, IKontroler)


@pytest.mark.parametrize(
    "kontroler, test_input, expected_output", controllers_with_old_views
)
def test_kontroler_handling(kontroler, test_input, expected_output, capsys):
    kontroler.oblicz(test_input)
    kontroler.wyswietl()
    captured = capsys.readouterr()
    assert captured.out == get_mock(expected_output)
