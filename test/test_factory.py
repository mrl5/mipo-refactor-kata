# SPDX-License-Identifier: BSD-2-Clause

import pytest

from src.errory import NieznanaUmowaError
from src.factory import dajUmowe
from src.umowy.dzielo import UmowaDzielo
from src.umowy.praca import UmowaPraca
from src.umowy.zlecenie import UmowaZlecenie

umowy = [("P", UmowaPraca), ("Z", UmowaZlecenie), ("D", UmowaDzielo)]


@pytest.mark.parametrize("userInput, expectedObject", umowy)
def test_factory(userInput, expectedObject):
    assert isinstance(dajUmowe(userInput), expectedObject)


def test_factory_exceptions():
    with pytest.raises(NieznanaUmowaError):
        dajUmowe("")
    with pytest.raises(NieznanaUmowaError):
        dajUmowe(" P")
    with pytest.raises(NieznanaUmowaError):
        dajUmowe("z")
    with pytest.raises(NieznanaUmowaError):
        dajUmowe("Praca")
    with pytest.raises(NieznanaUmowaError):
        dajUmowe("X")
