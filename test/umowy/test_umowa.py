# SPDX-License-Identifier: BSD-2-Clause

import pytest

from src.umowy.umowa import Umowa


def test_cant_instantiate():
    with pytest.raises(TypeError):
        Umowa()
