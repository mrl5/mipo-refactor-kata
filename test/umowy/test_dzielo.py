# SPDX-License-Identifier: BSD-2-Clause

import pytest
from jsonschema import validate

from src.interfejsy.serializer import ISerializer
from src.interfejsy.umowa import IUmowa
from src.kontrakty.schematy import pobierzSchematJson
from src.umowy.dzielo import UmowaDzielo
from src.umowy.umowa import Umowa


@pytest.fixture(scope="function")
def instance():
    o = UmowaDzielo()
    o.ustawWynagrodzenieBrutto(1337)
    return o


def test_inheritance(instance):
    assert isinstance(instance, Umowa) is True


def test_interface_implementation(instance):
    assert isinstance(instance, IUmowa) is True
    assert isinstance(instance, ISerializer) is True


def test_correct_value(instance):
    instance.obliczWynagrodzenieNetto()
    result = instance.wynagrodzenieNetto
    assert round(result, 2) == result
    assert result == 1144.00


def test_serialization(instance):
    instance.obliczWynagrodzenieNetto()
    a_dict = instance.serializujSie()
    schemat = pobierzSchematJson(instance.kontrakt)
    assert validate(instance=a_dict, schema=schemat) is None
