# SPDX-License-Identifier: BSD-2-Clause

import json
from pathlib import Path
from typing import Union

import pytest
from jsonschema import ValidationError

from src.interfejsy.widok import IWidok
from src.widoki.cli_stary import CliStaryWidok
from src.widoki.widok import Widok

PRACA_MOCK = "mock_cli_stary_praca.txt"
ZLECENIE_MOCK = "mock_cli_stary_zlecenie.txt"
SERIALIZED_PRACA_MOCK = "mock_serialized_praca.json"
SERIALIZED_ZLECENIE_MOCK = "mock_serialized_zlecenie.json"


def get_mock(filename: str) -> Union[str, dict]:
    mocks_path = Path(__file__).resolve().parent
    with open(mocks_path / filename) as f:
        if filename.endswith(".txt"):
            mock = f.read()
        elif filename.endswith(".json"):
            mock = json.load(f)
    return mock


@pytest.fixture(scope="function")
def instance_praca():
    zrzutowanyModel = get_mock(SERIALIZED_PRACA_MOCK)
    o = CliStaryWidok(zrzutowanyModel)
    return o


@pytest.fixture(scope="function")
def instance_zlecenie():
    o = CliStaryWidok()
    zrzutowanyModel = get_mock(SERIALIZED_ZLECENIE_MOCK)
    o.model = zrzutowanyModel
    return o


@pytest.fixture(scope="function")
def instance_niewspierane():
    zrzutowanyModel = {"zonk": "!"}
    o = CliStaryWidok(zrzutowanyModel)
    return o


def test_inheritance(instance_praca, instance_zlecenie):
    assert isinstance(instance_praca, Widok) is True
    assert isinstance(instance_zlecenie, Widok) is True


def test_interface_implementation(instance_praca, instance_zlecenie):
    assert isinstance(instance_praca, IWidok) is True
    assert isinstance(instance_zlecenie, IWidok) is True


def test_walidacja(instance_niewspierane):
    with pytest.raises(ValidationError):
        instance_niewspierane.wyswietl()


def test_widok_praca(instance_praca, capsys):
    instance_praca.wyswietl()
    captured = capsys.readouterr()
    assert captured.out == get_mock(PRACA_MOCK)


def test_widok_zlecenie(instance_zlecenie, capsys):
    instance_zlecenie.wyswietl()
    captured = capsys.readouterr()
    assert captured.out == get_mock(ZLECENIE_MOCK)
